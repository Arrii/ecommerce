<?php
require_once('conn.php');
class product{
	public $db;
	public function __construct(){
		$connect = new dataBaseConnect();
		$this->db=$connect->conn();

	}
    /**
    * productDesc
    * arrange product 
    * @return : product in descending order
    */
	public function productDesc(){
		$sql= "SELECT * FROM product ORDER BY created DESC LIMIT 5";
		$res=$this->db->query($sql);
		while($result=$res->fetch_assoc()){
			$data[] = $result;

		}
		return $data;

	}
		public function navigation(){
		$sql= "SELECT * FROM slug "; 
		$res = $this->db->query($sql);
		while($result=$res->fetch_assoc()){
			$data[] = $result;
		}
		//print_r($data);die();
		return $data;
	}
	public function navigationDetail($id){
	   $data = array();
		$sql= "SELECT * FROM slug WHERE id=$id "; 
		$res = $this->db->query($sql);
		while($result=$res->fetch_assoc()){
			$data[] = $result;
		}
		//print_r($data);die();
		return $data;
	}
	public function searchProduct($search){
		$data = array();
	$searchq = preg_replace("#[^0-9a-z]#i","",$search);
	$sql="SELECT * FROM product WHERE name LIKE '%$searchq%' OR description LIKE '%$searchq%' OR img LIKE '%$searchq%'";
	$res= $this->db->query($sql);
	while($result=$res->fetch_assoc()){
		$data[] = $result;
	}
	//print_r($data); die;
	return $data;
	}

	public function showProducts(){
		$sql = "SELECT * FROM product";
		$res = $this->db->query($sql);
		while($result = $res->fetch_assoc()){
			$data[] = $result;
		}
		return $data;
	}
	public function showCart(){
		$sql =  "SELECT * FROM cart";
		$res = $this->db->query($sql);
		while($result = $res->fetch_assoc()){
			$data[] = $result;
		}
		//print_r($sql);die();
		return $data;
	}

	public function showProductDetail($product_id){
		$data = array();
		$sql = "SELECT * FROM product WHERE id=$product_id";
		$res = $this->db->query($sql);
		while($result = $res->fetch_assoc()){
			$data[] = $result;
		}
		//print_r($data);die();
		return $data;
	}

		    /**
    * deleteProduct
    * arrange product in cart 
    * @param $delete
    * @return : delete product form cart 
    */
	public function deleteProduct($delete){
		$sql = "DELETE FROM cart WHERE id=$delete";
		//print_r($sql);die();
		$result = $this->db->query($sql);
	if($result){
		header('location: productcart.php');
	}else{
		echo mysql_error($this->db);
	}
}

	public function viewCart($user_id){
		$data = array();
		$sql = "SELECT * FROM product WHERE id=$add_cart";
		$res = $this->db->query($sql);
		while($result = $res->fetch_assoc()){
			$data[] = $result;
		}
		return $data;

	}
	    /**
    * insertCart
    * add cart 
    * @param $userid,$productid,$quantity
    * @return : add product in cart
    */

	    public function insertCart($userid,$productid,$quantity){

	    	$status = 1;
	    	$created = date('Y-m-d H:i:s');
	    	$modified = date('Y-m-d H:i:s');


    	$sql="INSERT INTO cart(`userid`,`productid`,`quantity`,`created`,`modified`,`status`) VALUES('".$_SESSION['customer_id']."','".$productid."','".$quantity."','".$created."','".$modified."','".$status."')";
    	//print_r($sql);die;

    	$result = $this->db->query($sql);
    	if($result){
    		header('Location: show.php');
    		return $result;
    	}else{
    		echo mysql_error($this->db);
    	}
    }

   /**
    * cart
    * product add in cart
    * @param customer_id
    * @return : product lists of login user
    */
	public function cart($customer_id){
		$data = array();		
		$sql= "SELECT * FROM product as P  left JOIN cart as C ON P.id = C.productid WHERE C.userid=" . $customer_id;
		//echo $sql; die;
		$res = $this->db->query($sql);
		while($result = $res->fetch_assoc()){
			$data[] = $result;
		}
		return $data;

	}
}
	
/*$arrii = new product();
$chauhan=$arrii->showCart();
echo '<pre>';
print_r($chauhan);die();*/

?>