
<?php
include_once('header.php');
include_once('product_function.php');
$result = new product();
$all_cart_detail=$result->cart($_SESSION['customer_id']);

?>



<div class="container">
<div class="row">
<div class="col-md-10">
<table class="table_product_cart">
	<thead>
	<h2 style="text-align:center;">My Cart</h2>
	<tr>
	    <th class="heading_product_cart">Image</th>
		<th class="heading_product_cart">Name</th>
		<th class="heading_product_cart">Text-area</th>
		<th class="heading_product_cart">Price</th>
		<th class="heading_product_cart">Quantity</th>
		<th  class="heading_product_cart" colspan="2">Action</th>
		</tr>
	</thead>
	<tbody>
	<?php foreach ($all_cart_detail as $row) { ?>
		<tr>
		   <td class="heading_product_cart "><img class ="img-thumbnail cart_img" src="Admin/img/<?php echo $row['img'];?>"></td>
			<td class="heading_product_cart"><?php echo $row['name']; ?></td>
			<td class="heading_product_cart"><?php echo $row['description']; ?></td>
			<td class="heading_product_cart"><?php echo $row['price']; ?></td>
			<td class="heading_product_cart"><?php echo $row['quantity']; ?></td>
			<td class="form-group heading_product_cart"> 
				<button type="button" class="btn btn-default text-color"><a href="remove.php?delete=<?php echo $row['id']; ?>">REMOVE</a></button>
				
				
			</td>
		</tr>

		
	</tbody>
	<?php } ?>
</table>
</div>
<div class="row">
<div class="col-md-2">
<table class="table_product_cart">
<thead>
  <tr>
	<th class="place_order_control">Price</th>
	<th class="place_order_control">Delivery Charges</th>
	<th class="place_order_control">Amount Payable</th>
  </tr>
</thead>
<tbody>
	<tr>
		<td class="place_order_control">$56.00</td>
		<td class="place_order_control">Free</td>
		<td class="place_order_control">$56.00</td>
	</tr>
</tbody>
</table>
<button type="button" class="btn btn-default"><a href="delivery_form.php">Placeorder</a></button>
<!-- <form  method="post" action="order.php">

<input name="submit_order" type="submit" value="Place Order">

</form> -->

</div>
</div>
</div>
</div>
<?php include('footer.php'); ?>