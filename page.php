<!DOCTYPE html>
<html>
<head>
	<title>Page</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
	<script src="js/jquery.js"></script>
	<script src="//cdn.ckeditor.com/4.8.0/standard/ckeditor.js"></script>
	<script src="js/bootstrap.min.js"></script>

</head>
<body>
<?php
include('header.php');
?>
	<div class="container">
	<div class="row">
		<div class="col-md-12">
		<h2 style="text-align:center;">Page</h2>
<form class="form-group" action="aftpage.php" method="post"  style="border: 2px solid gray;padding:20px;">
		<label>Slug:</label>
		<input type="text" name="slug" class="form-control">
		<label>Name:</label>
		<input type="text" name="name" class="form-control">
		<label>Content:</label>
		<textarea class="form-control ckeditor" name="content"></textarea>
        <label>Status:</label>
		<input type="checkbox" name="status" class="form-control" style="width:20px;">
        <button type="submit" name="save" class="btn btn-primary">Save</button>
</form>
</div>
</div>
</div>
<?php
include('footer.php');
?>

</body>
</html>