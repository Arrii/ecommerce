<?php 
require_once('header.php');
require_once('afteditinfo.php');
require_once('User_Detail.php');

$edit_result = new userLogIn();
$edit_user_information = $edit_result->userInformation($_SESSION['customer_id']);



 ?>

	<div class="container">
	<div class="row">
		<div class="col-md-4">
	<ul class="list-group" style="margin-top:60px;">
  <li class="list-group-item"><a href="editinfo.php">Edit Info</a>
  </li>
   <li class="list-group-item"><a href="pass.php">Change Password</a>
  </li>
</ul>
	</div>
		<div class="col-md-8">
				<?php
	foreach ($edit_user_information as $row) {
	?>
		<h2 style="text-align:center;">Update Info</h2>
<form class="form-group" action="afteditinfo.php" method="POST">
<input type="hidden" name="id" value="<?php echo $row['id']; ?>">
		<label>Username:</label>
		<input class="form-control"  type="text" name="username" value="<?php echo $row['username']; ?>">
		<label>Email:</label>
		<input class="form-control"  type="email" name="email" value="<?php echo $row['email']; ?>">
		<label>Mobile:</label>
		<input class="form-control"  type="number" name="mobile" value="<?php echo $row['mobile']; ?>">
		<label>Status:</label>
		<input style="width:20px;" class="form-control"  type="checkbox" name="status" value="<?php echo $row['status']; ?>">
		<button class="btn btn-primary"  type="submit" name="update">Update</button>
</form>
</div>
<?php
}
?>
</div>
<?php
include('footer.php');
?>
