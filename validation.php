<?php
class validation{
	public $valid = false;
	public function validName($value,$type='name'){
		if($type == 'name'){
			if(!preg_match("/^[a-zA-Z ]*$/",$value)){
				$this->valid=false;
				return "Invalid username";
				
			}else{
				$this->valid=true;
				return "valid username";
				
			}

		}

	}
	public function validPassword($value,$type='password'){
		if($type == 'password'){
			if(!preg_match("/^[a-zA-Z0-9+&@]*$/",$value)){
				return "Invalid password";
				$this->valid=false;
			}else{
				return "valid password";
				$this->valid=true;
			}
		}
	}
	public function validEmail($value,$type='email'){
		if($type == 'email'){
			if(!filter_var($value,FILTER_VALIDATE_EMAIL)){
				return "invaild email";
				$this->valid=false;
			}else{
				return "valid email";
				$this->valid=true;
			}
		}
	}
	public function validNumber($value,$type='number'){
		if($type == 'number'){
			if(!preg_match("/^[0-9]*$/",$value)){
				return "invalid number";
				$this->valid=false;
			}else{
				return "valid number";
				$this->valid=true;
			}
		}
	}
}
/*$res=new validation();
$arr = $res->validNumber('1234567890456789','number');
print_r($arr);
?>*