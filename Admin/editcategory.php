		
<?php 
include('product_function.php');
$result = new adminProduct();
$user = $result->showUserName();
$admin = $user['Admin'];


//collect all data of category with proper category id
$category_id = isset($_REQUEST['category_id']) ? $_REQUEST['category_id'] : 0;
if($category_id > 0){
	if($_REQUEST['action'] == 'edit'){
		$category = $result->adminCategoryDetail($category_id);
	}else if($_REQUEST['action'] == 'delete'){
		$result->deleteCategory($category_id);
		header('location: managecategory.php');

	}
}



//update the category
if(isset($_POST['name']) && !empty($_POST['name'])){
	$category_data['name']        = isset($_POST['name']) ? $_POST['name'] : '';
    $category_data['description'] = isset($_POST['description']) ? $_POST['description'] : '';
    $category_data['slug']        = isset($_POST['slug']) ? $_POST['slug'] : '';
    $category_data['status']      = $_POST['status'];
    $category_data['id']          = isset($_POST['id']) ? $_POST['id'] : '';
    //print_r($category_data);die();
    $update_category_data = $result->updateCategory($category_data);
    if($update_category_data > 0){
    	header('location:managecategory.php');
    }else{
    	echo "mysqli_error($this->database)";
    }
}

?>
	<?php include('header.php'); ?>
	<div id="sidebar-collapse" class="col-sm-3 col-lg-2 sidebar">
		<div class="profile-sidebar">
			<div class="profile-userpic">
				<img src="pro.jpg" class="img-responsive" alt="">
			</div>
			<div class="profile-usertitle">
				<div class="profile-usertitle-name"><?php echo $admin; ?></div>
			</div>
			<div class="clear"></div>
		</div>
		<div class="divider"></div>
		<form role="search">
			<div class="form-group">
				<input type="text" class="form-control" placeholder="Search">
			</div>
		</form>
		<ul class="nav menu">
			<li class="active"><a href="index.php"><em class="fa fa-dashboard">&nbsp;</em> Dashboard</a></li>
			<li class="parent "><a data-toggle="collapse" href="#sub-item-1">
				<em class="fa fa-navicon">&nbsp;</em> PRODUCTS <span data-toggle="collapse" href="#sub-item-1" class="icon pull-right"><em class="fa fa-plus"></em></span>
				</a>
				<ul class="children collapse" id="sub-item-1">
					<li><a class="" href="manage.php">
						<span class="fa fa-arrow-right"></span> MANAGE PRODUCTS
					</a></li>
				</ul>
			</li>
			<li class="parent "><a data-toggle="collapse" href="#sub-item-2">
				<em class="fa fa-navicon">&nbsp;</em> USERS <span data-toggle="collapse" href="#sub-item-2" class="icon pull-right"><em class="fa fa-plus"></em></span>
				</a>
				<ul class="children collapse" id="sub-item-2">
					<li><a class="" href="manageuser.php">
						<span class="fa fa-arrow-right"></span> MANAGE USER
					</a></li>
				</ul>
			</li>
			<li class="parent "><a data-toggle="collapse" href="#sub-item-3">
				<em class="fa fa-navicon">&nbsp;</em> CATEGORY <span data-toggle="collapse" href="#sub-item-3" class="icon pull-right"><em class="fa fa-plus"></em></span>
				</a>
				<ul class="children collapse" id="sub-item-3">
					<li><a class="" href="managecategory.php">
						<span class="fa fa-arrow-right"></span> MANAGE CATEGORY
					</a></li>
				</ul>
			</li>
			<li><a href="logout.php"><em class="fa fa-power-off">&nbsp;</em> Logout</a></li>
		</ul>
	</div><!--/.sidebar-->
	
	<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
		<div class="row">
			
		<h2 style="text-align:center;">Update Information</h2>
		<?php foreach ($category as $row) { ?>	
<form class="form-group box" action="" method="post">
<input type="hidden" name="id" value="<?php echo $row['id']; ?>">
<label>Name:</label>
		<input class="form-control"  type="text" name="name" value="<?php echo $row['name']; ?>">
		<label>Description:</label>
		<input class="form-control"  type="text" name="description" value="<?php echo $row['description']; ?>">
		<label>Slug:</label>
		<input class="form-control"  type="text" name="slug" value="<?php echo $row['slug']; ?>">
		<label>Status:</label>
		<input style="width:20px;" class="form-control"  type="checkbox" name="status" value="<?php echo $row['status']; ?>">
		<button class="btn btn-primary"  type="submit" name="update">Update</button>
</form>
<?php } ?>
</div>

</div>

</div>

	  
<?php include('footer.php'); ?>
<script src="js/jquery-1.11.1.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<script src="js/chart.min.js"></script>
	<script src="js/chart-data.js"></script>
	<script src="js/easypiechart.js"></script>
	<script src="js/easypiechart-data.js"></script>
	<script src="js/bootstrap-datepicker.js"></script>
	<script src="js/custom.js"></script>
	

