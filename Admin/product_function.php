<?php
require_once('conn.php');
class adminProduct{
	public function __construct(){
		$result = new admimDataBaseConnection();
		$this->database=$result->connect();
	}
    /**
    * showTotalProduct
    * get product detail
    * @return : show the products with status 1
    */
	public function showTotalProduct(){
		$sql = "SELECT count(id) AS total_product FROM product WHERE status = 1";
		$res = $this->database->query($sql);
		return $result = $res->fetch_assoc();
	}
	/**
    * showTotalUser
    * get user detail
    * @return : show the customer details only
    */
	public function showTotalUser(){
		$sql = "SELECT count(id) AS total_user FROM register WHERE role_id = 3";
		$res = $this->database->query($sql);
		return $result = $res->fetch_assoc();
	}
	/**
    * showUserName
    * get admin detail
    * @return : show the admin detail
    */
	public function showUserName(){
		$sql = "SELECT username AS Admin FROM register WHERE role_id = 1";
		$res = $this->database->query($sql);
		return $result = $res->fetch_assoc();

	}
	/**
    * totalProducts
    * get total products
    * @return : collect all information about whole products
    */
	public function totalProducts(){
		$sql = "SELECT * FROM product";
		$res = $this->database->query($sql);
		while($result = $res->fetch_assoc()){
			$data[] = $result;
		}
		return $data;
	}
	/**
    * adminProductInformation
    * get product detail
    * @param   : product_id
    * @return : collect product with unique id 
    */
	public function adminProductInformation($product_id){
	$data = array();
	$sql = "SELECT * FROM product WHERE id=$product_id";
	//print_r($sql);die();
	$res = $this->database->query($sql);
	while($result = $res->fetch_assoc()){
	$data[] = $result;		
	}
	return $data;
	}
	/**
    * updateProduct
    * get update products
    * @param  : product_data(array)
    * @return : update the products  
    */
	public function updateProduct($product_data = array()){
		$img         = $product_data['img'];
		$category_id = $product_data['category_id'];
		$name        = $product_data['name'];
		$description = $product_data['description'];
		$price       = $product_data['price'];
		$quantity    = $product_data['quantity'];
		$status      = $product_data['status'];
		$id          = $product_data['id'];
		$sql = "UPDATE product SET 
							img='$img',
							category_id='$category_id',
							name='$name',
							description='$description',
							price='$price',
							quantity='$quantity',
							status='$status' 
							WHERE id='$id'";
							//print_r($sql);die();
		return $result = $this->database->query($sql);

	}
	 /**
    * adminShowProductDetail
    * get all agent tickets
    * @param $agent_data(array)
    * @return --------
    */
		public function adminShowProductDetail($product_id){
		$data = array();
		$sql = "SELECT * FROM product WHERE id=$product_id";
		$res = $this->database->query($sql);
		while($result = $res->fetch_assoc()){
			$data[] = $result;
		}
		return $data;
	}
    /**
    * deleteProduct
    * delete product
    * @param $delete_id
    * @return --------
    */
	public function deleteProduct($product_id){
		$sql = "DELETE FROM product WHERE id=$product_id";
		//print_r($sql);die();
		return $res = $this->database->query($sql);

	}
	/**
    * addCategory
    * add category
    * @param $data
    * @return show new category
    */
	public function addCategory($data){
		$sql="INSERT INTO category(`name`,`description`,`slug`,`status`) VALUES('".$data['name']."','".$data['description']."','".$data['slug']."','".$data['status']."')";
	    $res = $this->database->query($sql);
		 if($res > 0){
		 	header('Location: manageuser.php');
		 	return $res;
		 }else{
		 	echo mysqli_error($this->database);
		 }

	}
    /**
    * adminCategoryDetail
    * get category 
    * @param $edit_category_id
    * @return get category with proper id
    */

	public function adminCategoryDetail($category_id){
		$data = array();
		$sql = "SELECT * FROM category WHERE id=$category_id";
		$res = $this->database->query($sql);
		while($result = $res->fetch_assoc()){
			$data[] = $result;
		}
		return $data;
	}
	/**
    * updateCategory
    * update category 
    * @param $category_data
    * @return -------
    */
	public function updateCategory($category_data = array()){
		$name        = $category_data['name'];
		$description = $category_data['description'];
		$slug        = $category_data['slug'];
		$status      = $category_data['status'];
		$id          = $category_data['id'];

		$sql = "UPDATE category SET
		                        name='$name',
		                        description='$description',
		                        slug='$slug',
		                        status='$status'
		                         WHERE id='$id'";
		                         //print_r($sql);die();
		return $res = $this->database->query($sql);                         
	}
	/**
    * deleteCategory
    * delete category 
    * @param $delete_id
    * @return -------
    */
	public function deleteCategory($category_id){
		$sql = "DELETE FROM category WHERE id=$category_id";
		//print_r($sql);die();
		return $res = $this->database->query($sql);
	}
}
/*$res = new adminProduct();
$arrii = $res->deleteCategory('10');
print_r($arrii);die();*/

?>