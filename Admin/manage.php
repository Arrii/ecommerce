<?php
require_once('product_function.php');
$result = new adminProduct();
$admin_total_products = $result->totalProducts();
$user    = $result->showUserName();
$admin = $user['Admin'];
//print_r($products);die();

	
?>

	<?php include('header.php') ?>
	<div id="sidebar-collapse" class="col-sm-3 col-lg-2 sidebar">
		<div class="profile-sidebar">
			<div class="profile-userpic">
				<img src="pro.jpg" class="img-responsive" alt="">
			</div>
			<div class="profile-usertitle">
				<div class="profile-usertitle-name"><?php echo $admin; ?></div>
			</div>
			<div class="clear"></div>
		</div>
		<div class="divider"></div>
		<form role="search">
			<div class="form-group">
				<input type="text" class="form-control" placeholder="Search">
			</div>
		</form>
		<ul class="nav menu">
			<li class="active"><a href="index.php"><em class="fa fa-dashboard">&nbsp;</em> Dashboard</a></li>
			<li class="parent "><a data-toggle="collapse" href="#sub-item-1">
				<em class="fa fa-navicon">&nbsp;</em> PRODUCTS <span data-toggle="collapse" href="#sub-item-1" class="icon pull-right"><em class="fa fa-plus"></em></span>
				</a>
				<ul class="children collapse" id="sub-item-1">
					<li><a class="" href="manage.php">
						<span class="fa fa-arrow-right"></span> MANAGE PRODUCTS
					</a></li>
				</ul>
			</li>
			<li class="parent "><a data-toggle="collapse" href="#sub-item-2">
				<em class="fa fa-navicon">&nbsp;</em> USERS <span data-toggle="collapse" href="#sub-item-2" class="icon pull-right"><em class="fa fa-plus"></em></span>
				</a>
				<ul class="children collapse" id="sub-item-2">
					<li><a class="" href="manageuser.php">
						<span class="fa fa-arrow-right"></span> MANAGE USER
					</a></li>
				</ul>
			</li>
			<li class="parent "><a data-toggle="collapse" href="#sub-item-3">
				<em class="fa fa-navicon">&nbsp;</em> CATEGORY <span data-toggle="collapse" href="#sub-item-3" class="icon pull-right"><em class="fa fa-plus"></em></span>
				</a>
				<ul class="children collapse" id="sub-item-3">
					<li><a class="" href="managecategory.php">
						<span class="fa fa-arrow-right"></span> MANAGE CATEGORY
					</a></li>
				</ul>
			</li>
			<li><a href="logout.php"><em class="fa fa-power-off">&nbsp;</em> Logout</a></li>
		</ul>
	</div><!--/.sidebar-->
		
	<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
		<div class="row">
		
<table class="product_table">
	<thead>
	<h2 style="text-align:center;">Show Detail</h2>
	<a title="Add Product" href="detailform.php" class="btn btn-primary glyphicon_box"><span class="glyphicon glyphicon-plus"></span></a>
	<tr class="heading">
	    <th class="product_heading">Image</th>
        <th class="product_heading">Product Name</th>
		<th class="product_heading">Product Detail</th>
		<th class="product_heading">Price</th>
		<th class="product_heading">Quantity</th>
		<th class="product_heading">Created</th>
		<th class="product_heading">Modified</th>
		<th class="product_heading">Status</th>
		<th  class="product_heading" colspan="3">Action</th>
		</tr>
	</thead>
	<tbody>
	<?php
if(!empty($admin_total_products)){
	 foreach ($admin_total_products as $row)  { ?>
		<tr class="heading">
		   <td class="product_heading "><img class= "img_control" src="img/<?php echo $row['img'];?>"></td>
			<td class="product_heading"><?php echo $row['name']; ?></td>
			<td class="product_heading"><?php echo $row['description']; ?></td>
			<td class="product_heading"><?php echo $row['price']; ?></td>
			<td class="product_heading"><?php echo $row['quantity']; ?></td>
			<td class="product_heading"><?php echo $row['created']; ?></td>
			<td class="product_heading"><?php echo $row['modified']; ?></td>
			<td class="product_heading"><?php echo $row['status']; ?></td>
			<td class="product_heading">
				<a title = "Update" href="manageupdate.php?action=edit&product_id=<?php echo $row['id']; ?>"><span class="fa fa-pencil-square-o"></span></a>
			</td>
			<td class="product_heading">
				<a title= "View Detail" href="manageview.php?product_id=<?php echo $row['id']; ?>"><span class="fa fa-eye"></span></a>
			</td>
			<td class="form-group product_heading"> 
				<a title="Delete" onclick = "return checkDelete()" href="manageupdate.php?action=delete&product_id=<?php echo $row['id']; ?>"><span class="fa fa-trash-o"></span></a>
			</td>
		</tr>
		<?php }
	} ?>
	</tbody>
</table>	
	</div>
	</div>
	<?php include('footer.php'); ?>
	
	  

<script src="js/jquery-1.11.1.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<script src="js/chart.min.js"></script>
	<script src="js/chart-data.js"></script>
	<script src="js/easypiechart.js"></script>
	<script src="js/easypiechart-data.js"></script>
	<script src="js/bootstrap-datepicker.js"></script>
	<script src="js/custom.js"></script>
		<script type="text/javascript">
		function checkDelete(){
        return confirm('Are you sure?');
		}
	</script>
