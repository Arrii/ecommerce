		
<?php 
require_once('product_function.php');
require_once('admin_function.php');
//show the category avilable
    $category = new adminUser();
    $product_category = $category->productCategory();


//show the admin name
    $detail = new adminProduct();
    $user    = $detail->showUserName();
    $admin = $user['Admin'];

//show the fetch data & delete the product
    $product_id = isset($_REQUEST['product_id']) ? $_REQUEST['product_id'] : 0;
    if($product_id > 0){
    	if($_REQUEST['action'] == 'edit'){
    	$product_information = $detail->adminProductInformation($product_id);
    	
    }else if($_REQUEST['action'] == 'delete'){
    	$detail->deleteProduct($product_id);
    	header('location: manage.php');
    }

    }
    



if(isset($_POST['id']) && !empty($_POST['id'])){
	$product_data = array();		
	if(isset($_FILES['img']) && !empty($_FILES['img'])){
	$img_name = basename($_FILES['img']['name']);
	$target = "img/".$img_name;
	move_uploaded_file($_FILES['img']['tmp_name'], $target);
	$img = $_FILES['img']['tmp_name'];
	//print_r($img);die();
	$product_data['img'] = $img_name;
    }else{
    	$product_data['img'] = '';
    }
	//$product_data['img'] = isset($_FILE['img']) ? $_FILE['img'] : '';
	$product_data['category_id'] = isset($_POST['category_id']) ? $_POST['category_id'] : '';
	$product_data['name'] = isset($_POST['name']) ? $_POST['name'] : '';
	$product_data['description'] = isset($_POST['description']) ? $_POST['description'] : '';
	$product_data['price'] = isset($_POST['price']) ? $_POST['price'] : '';
	$product_data['quantity'] = isset($_POST['quantity']) ? $_POST['quantity'] : '';
	$product_data['status'] = isset($_POST['status']);
	$product_data['id'] = isset($_POST['id']) ? $_POST['id'] : 0;
	//print_r($product_data);die();

	//$detail = new adminProduct();
    $update_product = $detail->updateProduct($product_data);
    		if($update_product){
			header('location: manage.php');
		}else{
			echo mysql_error($this->database);
		}
}

 ?>

<?php include('header.php'); ?>
	<div id="sidebar-collapse" class="col-sm-3 col-lg-2 sidebar">
		<div class="profile-sidebar">
			<div class="profile-userpic">
				<img src="pro.jpg" class="img-responsive" alt="">
			</div>
			<div class="profile-usertitle">
				<div class="profile-usertitle-name"><?php echo $admin; ?></div>
			</div>
			<div class="clear"></div>
		</div>
		<div class="divider"></div>
		<form role="search">
			<div class="form-group">
				<input type="text" class="form-control" placeholder="Search">
			</div>
		</form>
		<ul class="nav menu">
			<li class="active"><a href="index.php"><em class="fa fa-dashboard">&nbsp;</em> Dashboard</a></li>
		<li class="parent "><a data-toggle="collapse" href="#sub-item-1">
				<em class="fa fa-navicon">&nbsp;</em> PRODUCTS <span data-toggle="collapse" href="#sub-item-1" class="icon pull-right"><em class="fa fa-plus"></em></span>
				</a>
				<ul class="children collapse" id="sub-item-1">
					<li><a class="" href="manage.php">
						<span class="fa fa-arrow-right"></span> MANAGE PRODUCTS
					</a></li>
				</ul>
			</li>
			<li class="parent "><a data-toggle="collapse" href="#sub-item-2">
				<em class="fa fa-navicon">&nbsp;</em> USERS <span data-toggle="collapse" href="#sub-item-2" class="icon pull-right"><em class="fa fa-plus"></em></span>
				</a>
				<ul class="children collapse" id="sub-item-2">
					<li><a class="" href="manageuser.php">
						<span class="fa fa-arrow-right"></span> MANAGE USER
					</a></li>
				</ul>
			</li>
			<li class="parent "><a data-toggle="collapse" href="#sub-item-3">
				<em class="fa fa-navicon">&nbsp;</em> CATEGORY <span data-toggle="collapse" href="#sub-item-3" class="icon pull-right"><em class="fa fa-plus"></em></span>
				</a>
				<ul class="children collapse" id="sub-item-3">
					<li><a class="" href="managecategory.php">
						<span class="fa fa-arrow-right"></span> MANAGE CATEGORY
					</a></li>
				</ul>
			</li>
			<li><a href="logout.php"><em class="fa fa-power-off">&nbsp;</em> Logout</a></li>
		</ul>
	</div><!--/.sidebar-->
		
	<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
		<div class="row">
		<h2 style="text-align:center;">Update</h2>
		<?php foreach ($product_information as $row) { ?>
<form class="form-group box" action="" method="post" enctype="multipart/form-data">
<input type="hidden" name="id" value="<?php echo $row['id']; ?>">
		<label>Image:</label>
 		<input class="form-control" type="file" name="img" value="<?php echo $row['img']; ?>">
 		<label>Category_id:</label>
			<select class="form-control" name="category_id" value="<?php echo $row['category_id']; ?>">
            <?php foreach($product_category as $product){ ?>
            <option value="<?php echo $product['id']; ?>"><?php echo $product['name']; ?></option>
            <?php } ?>
            </select>
		<label>Name:</label>
		<input class="form-control"  type="text" name="name" value="<?php echo $row['name']; ?>">
		<label>Text-area:</label>
		<input class="form-control"  type="text-area" name="description" value="<?php echo $row['description']; ?>">
		<label>Price:</label>
		<input class="form-control"  type="text" name="price" value="<?php echo $row['price']; ?>">
		<label>Quantity:</label>
		<input class="form-control"  type="number" name="quantity" value="<?php echo $row['quantity']; ?>">
		<label>Status:</label>
		<input style="width:20px;" class="form-control"  type="checkbox" name="status" value="<?php echo $row['status']; ?>">
		<button class="btn btn-primary"  type="submit" name="update">Update</button>
</form>
</div>
<?php } ?>
</div>
</div>
<?php include('footer.php'); ?>
	  

<script src="js/jquery-1.11.1.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<script src="js/chart.min.js"></script>
	<script src="js/chart-data.js"></script>
	<script src="js/easypiechart.js"></script>
	<script src="js/easypiechart-data.js"></script>
	<script src="js/bootstrap-datepicker.js"></script>
	<script src="js/custom.js"></script>

