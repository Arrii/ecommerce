<?php
require_once('admin_function.php');
require_once('config.php');
if(isset($_SERVER['REQUEST_METHOD']) && $_SERVER['REQUEST_METHOD'] == 'POST'){
	// print_r($_POST);die();
	if(isset($_POST['username'])){
		$username = $_POST['username'];
	}
	if(isset($_POST['password'])){
		$password = $_POST['password'];
	}
	if(!empty($username) && !empty($password) ){
		$data = new adminUser();
		$admin_login = $data->adminLogIn($username,$password);
		//print_r($admin_login);die();
	}
	if($admin_login['role_id'] == ROLE_ADMIN){
		session_start();
		$_SESSION['username'] = $username;
		$_SESSION['admin_id'] = $admin_login['id'];
		$_SESSION['role_id'] = $admin_login['role_id'];
		
		header('location: index.php');
	}else{
		header('location: login.php');
	}
}
?>
<!DOCTYPE html>
<html>
<head>
	<title>Login</title>

<!-- 	<script>

function login() {
    var x = document.myForm.username.value;
	  var y = document.myForm.password.value;
    if (x==null || x=="") {
        alert("user must be filled out");
        return false;
    }
	if (y==null || y=="") {
        alert("Password must be filled out");
        return false;
    }
}
</script> -->

</head>
<body>
<?php include('header.php'); ?>
	<div class="container">
	<div class="row">
	<div class="col-md-3"></div>
		<div class="col-md-6">
		
<form class="form-group form_control" action="" method="post" name="myForm" >
	<h3>Admin Login</h3>
		<label>Username:</label>
		<input type="text" name="username" class="form-control">
		<label>Password:</label>
		<input type="password" name="password" class="form-control">
        <br>
        <button type="submit" class="btn btn-primary" name="login"/>Save</button>
</form>
</div>
<div class="col-md-3"></div>
</div>
</div>
<?php include('footer.php'); ?>
</body>
</html>