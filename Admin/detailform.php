<?php
require_once('product_function.php');
require_once('admin_function.php');
$result = new adminUser();
$detail_of_total_category = $result->productCategory();
$results = new adminProduct();
$user    = $results->showUserName();
$admin = $user['Admin'];

if(isset($_POST['name']) && !empty($_POST['name'])){
  	 if(isset($_FILES['img']) && !empty($_FILES['img'])){
  		$target = "img/".basename($_FILES['img']['name']);
		move_uploaded_file($_FILES['img']['tmp_name'],$target);
        $img = $_FILES['img']['name'];
        $data['img'] = $img;
  	} else {
        $data['img'] = '';
 	}
	
	$data['category_id'] = (isset($_POST['category_id']) && !empty($_POST['category_id'])) ? $_POST['category_id'] : '';
	
	$data['name'] = (isset($_POST['name']) && !empty($_POST['name'])) ? $_POST['name'] : '';
	
	$data['description'] = (isset($_POST['description']) && !empty($_POST['description'])) ? $_POST['description'] : '';
	
	$data['price'] = (isset($_POST['price']) && !empty($_POST['price'])) ? $_POST['price'] : '';
	
	$data['quantity'] = (isset($_POST['quantity']) && !empty($_POST['quantity'])) ?  $_POST['quantity'] : '';
	
	$data['status'] = (isset($_POST['status']) && !empty($_POST['status'])) ?  $_POST['status'] : 0;
	
	$data['created'] = date('Y-m-d H:i:s');
	//print_r($data);die();
	$add_product = $result->addProduct($data);
	//print_r($add_product);die();
}

?>

	<?php include('header.php'); ?>
	<div id="sidebar-collapse" class="col-sm-3 col-lg-2 sidebar">
		<div class="profile-sidebar">
			<div class="profile-userpic">
				<img src="pro.jpg" class="img-responsive" alt="">
			</div>
			<div class="profile-usertitle">
				<div class="profile-usertitle-name"><?php echo $admin; ?></div>
			</div>
			<div class="clear"></div>
		</div>
		<div class="divider"></div>
		<form role="search">
			<div class="form-group">
				<input type="text" class="form-control" placeholder="Search">
			</div>
		</form>
		<ul class="nav menu">
			<li><a href="index.php"><em class="fa fa-dashboard">&nbsp;</em> Dashboard</a></li>
<li class="parent "><a data-toggle="collapse" href="#sub-item-1">
				<em class="fa fa-navicon">&nbsp;</em> PRODUCTS <span data-toggle="collapse" href="#sub-item-1" class="icon pull-right"><em class="fa fa-plus"></em></span>
				</a>
				<ul class="children collapse" id="sub-item-1">
					<li><a class="" href="manage.php">
						<span class="fa fa-arrow-right"></span> MANAGE PRODUCTS
					</a></li>
				</ul>
			</li>
			<li class="parent "><a data-toggle="collapse" href="#sub-item-2">
				<em class="fa fa-navicon">&nbsp;</em> USERS <span data-toggle="collapse" href="#sub-item-2" class="icon pull-right"><em class="fa fa-plus"></em></span>
				</a>
				<ul class="children collapse" id="sub-item-2">
					<li><a class="" href="manageuser.php">
						<span class="fa fa-arrow-right"></span> MANAGE USER
					</a></li>
				</ul>
			</li>
			<li class="parent "><a data-toggle="collapse" href="#sub-item-3">
				<em class="fa fa-navicon">&nbsp;</em> CATEGORY <span data-toggle="collapse" href="#sub-item-3" class="icon pull-right"><em class="fa fa-plus"></em></span>
				</a>
				<ul class="children collapse" id="sub-item-3">
					<li><a class="" href="managecategory.php">
						<span class="fa fa-arrow-right"></span> MANAGE CATEGORY
					</a></li>
				</ul>
			</li>
			<li><a href="logout.php"><em class="fa fa-power-off">&nbsp;</em> Logout</a></li>
		</ul>
	</div><!--/.sidebar-->
		
	<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
		<div class="row">
		<h2 style="text-align:center;">Detail</h2>
<form class="form-group box" action="" method="post" enctype="multipart/form-data">
		<label>Image:</label>
		<input type="file" name="img" class="form-control">
		<label>Categories:</label>
	<select class="form-control" name="category_id">
<option value="0">Select</option>
<?php foreach($detail_of_total_category as $product){ ?>
<option value="<?php echo $product['id']; ?>"><?php echo $product['name']; ?></option>
<?php } ?>
</select>
		<label>Name:</label>
		<input type="text" name="name" class="form-control">
		<label>Text-area:</label>
		<input type="text-area" name="description" class="form-control">
        <label>Price:</label>
		<input type="text" name="price" class="form-control">
        <label>Quantity:</label>
		<input type="number" name="quantity" class="form-control">
        <label>Status:</label>
		<input type="checkbox" name="status" class="form-control" value = "1" style="width:20px;">
        <button type="submit" name="save" class="btn btn-primary">Save</button>
</form>
	</div>
	</div>
<?php include('footer.php'); ?>	
	  

<script src="js/jquery-1.11.1.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<script src="js/chart.min.js"></script>
	<script src="js/chart-data.js"></script>
	<script src="js/easypiechart.js"></script>
	<script src="js/easypiechart-data.js"></script>
	<script src="js/bootstrap-datepicker.js"></script>
	<script src="js/custom.js"></script>
