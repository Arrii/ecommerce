<?php
require_once('product_function.php');
require_once('admin_function.php');
$result = new adminUser();

//get the admin name
$users = new adminProduct();
$user = $users->showUserName();
$admin = $user['Admin'];


//print_r($_POST);die();
if(isset($_REQUEST['username']) && !empty($_REQUEST['username'])){

	$data['role_id']   = (isset($_POST['role_id']) && !empty($_POST['role_id'])) ? $_POST['role_id'] : '';

	$data['username']  = (isset($_POST['username']) && !empty($_POST['username'])) ? $_POST['username'] : '';

	$data['email']     = (isset($_POST['email']) && !empty($_POST['email'])) ? $_POST['email'] : '';

	$data['password']  = (isset($_POST['password']) && !empty($_POST['password'])) ? $_POST['password'] : '';

	$data['mobile']    = (isset($_POST['mobile']) && !empty($_POST['mobile'])) ? $_POST['mobile'] : '';

	$data['status']    = (isset($_POST['status']) && !empty($_POST['status'])) ? $_POST['status'] : 0;
	//print_r($data);die();
	$user_information = $result->addUser($data);
}
?>

<?php include('header.php'); ?>
	<div id="sidebar-collapse" class="col-sm-3 col-lg-2 sidebar">
		<div class="profile-sidebar">
			<div class="profile-userpic">
				<img src="pro.jpg" class="img-responsive" alt="">
			</div>
			<div class="profile-usertitle">
				<div class="profile-usertitle-name"><?php echo $admin; ?></div>
			</div>
			<div class="clear"></div>
		</div>
		<div class="divider"></div>
		<form role="search">
			<div class="form-group">
				<input type="text" class="form-control" placeholder="Search">
			</div>
		</form>
		<ul class="nav menu">
			<li class="active"><a href="index.php"><em class="fa fa-dashboard">&nbsp;</em> Dashboard</a></li>
<li class="parent "><a data-toggle="collapse" href="#sub-item-1">
				<em class="fa fa-navicon">&nbsp;</em> PRODUCTS <span data-toggle="collapse" href="#sub-item-1" class="icon pull-right"><em class="fa fa-plus"></em></span>
				</a>
				<ul class="children collapse" id="sub-item-1">
					<li><a class="" href="manage.php">
						<span class="fa fa-arrow-right"></span> MANAGE PRODUCTS
					</a></li>
				</ul>
			</li>
			<li class="parent "><a data-toggle="collapse" href="#sub-item-2">
				<em class="fa fa-navicon">&nbsp;</em> USERS <span data-toggle="collapse" href="#sub-item-2" class="icon pull-right"><em class="fa fa-plus"></em></span>
				</a>
				<ul class="children collapse" id="sub-item-2">
					<li><a class="" href="manageuser.php">
						<span class="fa fa-arrow-right"></span> MANAGE USER
					</a></li>
				</ul>
			</li>
			<li class="parent "><a data-toggle="collapse" href="#sub-item-3">
				<em class="fa fa-navicon">&nbsp;</em> CATEGORY <span data-toggle="collapse" href="#sub-item-3" class="icon pull-right"><em class="fa fa-plus"></em></span>
				</a>
				<ul class="children collapse" id="sub-item-3">
					<li><a class="" href="managecategory.php">
						<span class="fa fa-arrow-right"></span> MANAGE CATEGORY
					</a></li>
				</ul>
			</li>
			<li><a href="logout.php"><em class="fa fa-power-off">&nbsp;</em> Logout</a></li>
		</ul>
	</div><!--/.sidebar-->
		
	<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
		<div class="row">
	<h3 style="text-align: center;">Add User</h3>	
<form class="form-group box" action=""  method="post">

<label>Role:</label>
<select class="form-control" name="role_id">
<option value="0">Select</option>
<option value="3">Customer</option>
<option value="1">admin</option>
</select>
<label>Username:</label>
		<input type="text" name="username" class="form-control">
			<label>Email:</label>
		<input type="email" name="email" class="form-control" >
		<label>Password:</label>
		<input type="password" name="password" class="form-control">
				<label>Phone No.:</label>
		<input type="number" name="mobile" class="form-control">
        <label>Status:</label>
		<input type="checkbox" name="status" class="form-control"  value="1"style="width:20px;">
        <button type="submit" name="submit" class="btn btn-primary">Save</button>
</form>
	</div>
	</div>
	
	<?php include('footer.php'); ?>  

<script src="js/jquery-1.11.1.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<script src="js/chart.min.js"></script>
	<script src="js/chart-data.js"></script>
	<script src="js/easypiechart.js"></script>
	<script src="js/easypiechart-data.js"></script>
	<script src="js/bootstrap-datepicker.js"></script>
	<script src="js/custom.js"></script>
	

