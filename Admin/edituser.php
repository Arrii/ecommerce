		
<?php 
include('admin_function.php');
include('product_function.php');
$results = new adminUser();
$result = new adminProduct();
$user = $result->showUserName();
$admin = $user['Admin'];



//collect all data of category with proper category id

$user_id = isset($_REQUEST['user_id']) ? $_REQUEST['user_id'] : 0;
if($user_id > 0){
	if($_REQUEST['action'] == 'edit'){
		$user_information = $results->userInformation($user_id);
	}else if($_REQUEST['action'] == 'delete'){
		$results->deleteUser($user_id);
		header('location: manageuser.php');

	}
}



//update the category
if(isset($_POST['username']) && !empty($_POST['username'])){
	$user_data['username']    = isset($_POST['username']) ? $_POST['username'] : '';
    $user_data['email'] = isset($_POST['email']) ? $_POST['email'] : '';
    $user_data['password']        = isset($_POST['password']) ? $_POST['password'] : '';
    $user_data['mobile']        = isset($_POST['mobile']) ? $_POST['mobile'] : '';
    $user_data['status']      = $_POST['status'];
    $user_data['id']          = isset($_POST['id']) ? $_POST['id'] : '';
    //print_r($user_data);die();
    $update_category_data = $results->updateUserInformation($user_data);
    if($update_category_data > 0){
    	header('location:manageuser.php');
    }else{
    	echo mysqli_error($this->database);
    }
}

 ?>

	<?php include('header.php') ?>
	<div id="sidebar-collapse" class="col-sm-3 col-lg-2 sidebar">
		<div class="profile-sidebar">
			<div class="profile-userpic">
				<img src="pro.jpg" class="img-responsive" alt="">
			</div>
			<div class="profile-usertitle">
				<div class="profile-usertitle-name"><?php echo $admin; ?></div>
			</div>
			<div class="clear"></div>
		</div>
		<div class="divider"></div>
		<form role="search">
			<div class="form-group">
				<input type="text" class="form-control" placeholder="Search">
			</div>
		</form>
		<ul class="nav menu">
			<li class="active"><a href="index.php"><em class="fa fa-dashboard">&nbsp;</em> Dashboard</a></li>
			<li class="parent "><a data-toggle="collapse" href="#sub-item-1">
				<em class="fa fa-navicon">&nbsp;</em> PRODUCTS <span data-toggle="collapse" href="#sub-item-1" class="icon pull-right"><em class="fa fa-plus"></em></span>
				</a>
				<ul class="children collapse" id="sub-item-1">
					<li><a class="" href="manage.php">
						<span class="fa fa-arrow-right"></span> MANAGE PRODUCTS
					</a></li>
				</ul>
			</li>
			<li class="parent "><a data-toggle="collapse" href="#sub-item-2">
				<em class="fa fa-navicon">&nbsp;</em> USERS <span data-toggle="collapse" href="#sub-item-2" class="icon pull-right"><em class="fa fa-plus"></em></span>
				</a>
				<ul class="children collapse" id="sub-item-2">
					<li><a class="" href="manageuser.php">
						<span class="fa fa-arrow-right"></span> MANAGE USER
					</a></li>
				</ul>
			</li>
			<li class="parent "><a data-toggle="collapse" href="#sub-item-3">
				<em class="fa fa-navicon">&nbsp;</em> CATEGORY <span data-toggle="collapse" href="#sub-item-3" class="icon pull-right"><em class="fa fa-plus"></em></span>
				</a>
				<ul class="children collapse" id="sub-item-3">
					<li><a class="" href="managecategory.php">
						<span class="fa fa-arrow-right"></span> MANAGE CATEGORY
					</a></li>
				</ul>
			</li>
			<li><a href="logout.php"><em class="fa fa-power-off">&nbsp;</em> Logout</a></li>
		</ul>
	</div><!--/.sidebar-->
		
	<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
		<div class="row">
		<?php foreach ($user_information as $row) { ?>
		<h2 style="text-align:center;">Update Information</h2>
<form class="form-group box" action="" method="post">
<input type="hidden" name="id" value="<?php echo $row['id']; ?>">
<label>Role_id:</label>
		<input class="form-control"  type="text" name="role_id" value="<?php echo $row['role_id']; ?>">
<label>Username:</label>
		<input class="form-control"  type="text" name="username" value="<?php echo $row['username']; ?>">
		<label>Email:</label>
		<input class="form-control"  type="email" name="email" value="<?php echo $row['email']; ?>">
		<label>Password:</label>
		<input class="form-control"  type="text" name="password" value="<?php echo $row['password']; ?>">
		<label>Mobile:</label>
		<input class="form-control"  type="number" name="mobile" value="<?php echo $row['mobile']; ?>">
		<label>Status:</label>
		<input style="width:20px;" class="form-control"  type="checkbox" name="status" value="<?php echo $row['status']; ?>">
		<button class="btn btn-primary"  type="submit" name="update">Update</button>
</form>

<?php } ?>
</div>
</div>

<?php include('footer.php'); ?>	  

<script src="js/jquery-1.11.1.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<script src="js/chart.min.js"></script>
	<script src="js/chart-data.js"></script>
	<script src="js/easypiechart.js"></script>
	<script src="js/easypiechart-data.js"></script>
	<script src="js/bootstrap-datepicker.js"></script>
	<script src="js/custom.js"></script>
	
