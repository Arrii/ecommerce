<?php 
include('product_function.php');
$result = new product();
$product_id = isset($_REQUEST['product_id']) ? $_REQUEST['product_id'] : 0;
//print_r($product_id);die();
if($product_id > 0){
	$show_product_details = $result->showProductDetail($product_id);
	//print_r($show_product_details);die();
}

 ?>

<?php
include('header.php');
?>
<div class="container show_detail_box">
	<?php  
foreach ($show_product_details as $row)
{
?>
<div class="row">
<div class="col-md-4 ">
<img  class="show_detail_img" src="Admin/img/<?php echo $row['img'];?>">
</div>
<div class="col-md-8">

<br>
<hr>
<h3>Product Name:</h3>
<?php echo $row['name'];?><br>
<hr>
<h3>Price:</h3>
<?php echo $row['price'];?>
<hr>
<h3>Description:</h3>
<?php echo $row['description'];?>
<hr>
<button class="btn btn-primary" style="background-color: white;float:right;"><a href="cart.php?add_cart=<?php echo $row['id']; ?>" ><i class="glyphicon glyphicon-shopping-cart"></i></a></button>
</div>
</div>
</div> 
<?php
}
?>
</div>
<?php
include('footer.php');
?>