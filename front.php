<?php
    include('product_function.php');
    $product_result = new product();
    $product_data=$product_result->productDesc();
?>
<?php
include('header.php');
?>
<div class="container">
<div class="row">
<div class="col-md-3">
<ul class="list-group nav detail_control">
<h3>Category</h3>
  <li class="list-group-item"><a href="show.php">All Products</a>
  </li>
  <li class="list-group-item"><a href="editinfo.php">Account</a>
  </li>
</ul>
</div>
<div class="col-md-7">
<div id="c" class="carousel slide" data-ride="carousel">
<ol class="carousel-indicators">
<li data-target="#c" data-slide-to="0"></li>
<li data-target="#c" data-slide-to="1"></li>

</ol>
<div class="carousel-inner" role="listbox">
<div class="item active">
<img src="10.jpeg" class="img-thumbnail" style="float:left;width:800px;height:300px;">
  </div>
  <div class="item">
<img src="11.jpg" class="img-thumbnail" style="float:left;width:700px;height:300px;">
  </div>
</div>
</div>
<a href="#c" class="left carousel-control" data-slide="prev">
<span class="glyphicon glyphicon-chevron-left"></span>
                </a>
<a href="#c" class="right carousel-control" data-slide="next">
<span class="glyphicon glyphicon-chevron-right"></span>
</a>

</div>
<div class="col-md-2">
</div>
</div>
</div>
<div class="container">
 <div class="row">
   <div class="col-md-12 text-center">
   <h3>New Arrivals</h3>
       <hr style="width:20%;">
<div class="container text-center">
<?php 
  foreach($product_data as $select)
{
?>

<div class="arrival_control">
<img class="img-thumbnail arrival_img_control" src="Admin/img/<?php echo $select['img'];?>">
<div align="center"><?php echo $select['name'];?></div>
<div><?php echo $select['price'];?></div>
<div>

<button class="btn btn-default"><a href="cart.php?add_cart=<?php echo $select['id']; ?>" ><i class="glyphicon glyphicon-shopping-cart"></i></a></button>

<a href="showpro.php?product_id=<?php echo $select['id']; ?>"><button class="btn btn-primary">VIEW DETAIL</button></a>
    </div>
</div>
<?php
    }
?>          
 </div>
 </div>   
</div>
</div>
</div>
</div>
    <div class="container">
    <div class="row">
    <h3 style="text-align:center;">How It Works</h3>
    <hr>
    <div class="col-md-3 glyphicon_control">
    <h4><span class="glyphicon glyphicon-hourglass"></span><br>
    DIRECT MANUFACTURER</h4>
    </div>
        <div class="col-md-3 glyphicon_control">
    <h4><span class="glyphicon glyphicon-retweet"></span><br>
    EASY RETURNS</h4>
    </div>
        <div class="col-md-3 glyphicon_control">
    <h4><span class="glyphicon glyphicon-dashboard"></span><br>
    DOOR DELIVERY</h4>
    </div>
        <div class="col-md-3" >
    <h4><span class="glyphicon glyphicon-usd"></span><br>
    SECURE PAYMENT</h4>
    </div>
    </div>
    </div>

    <hr>
<?php
include('footer.php');
?>