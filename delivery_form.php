<?php 
require_once('header.php');
require_once('User_Detail.php');
$detail = new userLogIn();
if(isset($_REQUEST['productname']) && !empty($_REQUEST['productname'])){
	$data['customer_id'] = $_SESSION['customer_id'];
	$data['productname'] = isset($_POST['productname']) ? $_POST['productname'] : '';
	$data['phone'] = (isset($_POST['phone']) && !empty($_POST['phone'])) ? $_POST['phone'] : '';
	$data['pincode'] = (isset($_POST['pincode']) && !empty($_POST['pincode'])) ? $_POST['pincode'] : '';
	$data['address'] = (isset($_POST['address']) && !empty($_POST['address'])) ? $_POST['address'] : '';
	$data['state'] = (isset($_POST['state']) && !empty($_POST['from'])) ? $_POST['state'] : '';
	$data['status'] = (isset($_POST['status']) &&!empty($_POST['status']))	 ? $_POST['status'] : 0;

	$delivery_detail = $detail->userDeliveryDetail($data);
/*	if($delivery_detail > 0)
	{
		header('Location:address.php');
	}*/
	//print_r($delivery_detail);die();
}
?>
<div class="container">	
	<div class="row">
		<div class="col-md-8">
<form class="from-group box" action="" method="POST">
	<h5>DELIVERY ADDRESS</h5>
	<label>Name</label>
	<input type="text" name="productname" placeholder="Name" class="form-control">
	<label>Mobile No.</label>
	<input type="phone" name="phone" placeholder="Mobile Number" class="form-control">
	<label>Pincode</label> 
	<input type="text" name="pincode" placeholder="Pincode" class="form-control">
	<label>Address</label>
	<textarea class="form-control" rows="5" cols="10" name="address"></textarea>
	<label>State</label>
	<select class="form-control" name="state">
		<option>--Select--</option>
		<option value="Delhi">Delhi</option>
		<option value="Jaipur">Jaipur</option>
		<option value="Punjab">Punjab</option>
		<option value="Haryana">Haryana</option>
		<option value="Bihar">Bihar</option>
	</select>
	<label>Status</label>

	<input style="width: 20px;" type="checkbox" name="status" value="1" class="form-control">
	<button type="submit" name="save" class="btn btn-default">SAVE AND DELIVERY HERE</button>
</form>
</div>
<div class="row">
<div class="col-md-4">
	<table class="table_product_cart" style="float: right;">
<thead>
  <tr>
	<th class="place_order_control">Price</th>
	<th class="place_order_control">Delivery Charges</th>
	<th class="place_order_control">Amount Payable</th>
  </tr>
</thead>
<tbody>
	<tr>
		<td class="place_order_control">$56.00</td>
		<td class="place_order_control">Free</td>
		<td class="place_order_control">$56.00</td>
	</tr>
</tbody>
</table>
</div>	
</div>
</div>
</div>
<?php include('footer.php'); ?>


